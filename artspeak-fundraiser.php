<?php
/*
Plugin Name: ArtSpeak Fundraiser
Plugin URI: http://artspeakcreative.com
Description: Add functionality to display fundraiser progress bar
Version: 0.1
Author: Drew Sartorius
Author URI: http://artspeakcreative.com
License: GPL2
 */

//  PLUGIN FUNCTIONS
function ak_fundraiser_activation()
{
 // Run when plugin is activated
}
register_activation_hook(__FILE__, 'ak_fundraiser_activation');

function ak_fundraiser_deactivation()
{
 // Run when plugin is deactivated
}
register_deactivation_hook(__FILE__, 'ak_fundraiser_deactivation');

function ak_fundraiser_uninstall()
{
 // Run when plugin is uninstalled
}
register_uninstall_hook(__FILE__, 'ak_fundraiser_uninstall');

// Add settings to ACF fields
function ak_fundraiser_load_options()
{
 if (function_exists('acf_add_local_field_group')) {
  $group_key = 'ak_fundraiser_group';
  $group = array(
   'key' => $group_key,
   'title' => 'Fundraiser Options',
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'options_page',
      'operator' => '==',
      'value' => 'acf-options',
     ),
    ),
   ),
   'menu_order' => -1,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
   'active' => 1,
   'description' => '',
   'local' => 'php',
  );
  acf_add_local_field_group($group);

  // acf_add_local_field(array(
  //  'key' => $group_key . "_total_amount_text",
  //  'label' => 'Total Amount to Raise',
  //  'name' => 'ak_fundraiser_total',
  //  'type' => 'text',
  //  'parent' => $group_key,
  // ));

  // acf_add_local_field(array(
  //  'key' => $group_key . "_raised_amount_text",
  //  'label' => 'Amount Raised',
  //  'name' => 'ak_fundraiser_raised',
  //  'type' => 'text',
  //  'parent' => $group_key,
  // ));

  acf_add_local_field(array(
   'key' => $group_key . "_is_daily_true_false",
   'label' => 'Show Daily Goals',
   'name' => 'ak_fundraiser_is_daily',
   'type' => 'true_false',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_daily_goals_repeater",
   'label' => 'Daily Goals',
   'name' => 'ak_fundraiser_daily_goals',
   'type' => 'repeater',
   'sub_fields' => array(
    array(
     'key' => $group_key . "_day_name",
     'label' => "Day (Don't change)",
     'name' => 'day',
     'type' => 'text',
    ),
    array(
     'key' => $group_key . "_percent",
     'label' => 'Percent',
     'name' => 'percent',
     'type' => 'text',
    ),
    array(
     'key' => $group_key . "_people_count",
     'label' => 'People Count',
     'name' => 'people',
     'type' => 'text',
    ),
   ),
   'conditional_logic' => array(
    array(
     array(
      'field' => $group_key . "_is_daily_true_false",
      'operator' => '==',
      'value' => '1',
     ),
    ),
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_overall_header_text",
   'label' => 'Overall Goal Header Text',
   'name' => 'ak_fundraiser_header_text',
   'type' => 'text',
   'conditional_logic' => array(
    array(
     array(
      'field' => $group_key . "_is_daily_true_false",
      'operator' => '==',
      'value' => '0',
     ),
    ),
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_raised_percent_text",
   'label' => 'Overall Percent Raised',
   'name' => 'ak_fundraiser_percent',
   'type' => 'text',
   'conditional_logic' => array(
    array(
     array(
      'field' => $group_key . "_is_daily_true_false",
      'operator' => '==',
      'value' => '0',
     ),
    ),
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_is_showing_people_true_false",
   'label' => 'Show People Count',
   'name' => 'ak_fundraiser_is_showing_people',
   'type' => 'true_false',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_remaining_people_text",
   'label' => 'Overall People Count',
   'name' => 'ak_fundraiser_people_count',
   'type' => 'text',
   'conditional_logic' => array(
    array(
     array(
      'field' => $group_key . "_is_showing_people_true_false",
      'operator' => '==',
      'value' => '1',
     ),
     array(
      'field' => $group_key . "_is_daily_true_false",
      'operator' => '==',
      'value' => '0',
     ),
    ),
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_people_wording_text",
   'label' => 'People Wording Text',
   'name' => 'ak_fundraiser_people_wording',
   'type' => 'text',
   'conditional_logic' => array(
    array(
     array(
      'field' => $group_key . "_is_showing_people_true_false",
      'operator' => '==',
      'value' => '1',
     ),
    ),
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_last_updated_time",
   'label' => 'Last Updated At',
   'name' => 'ak_fundraiser_update_time',
   'type' => 'date_time_picker',
   'display_format' => 'Y-m-d H:i:s',
   'return_format' => 'F jS Y \a\t g:ia',
   'first_day' => 0,
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_style_text",
   'label' => 'Style',
   'name' => 'ak_fundraiser_style',
   'type' => 'text',
   'parent' => $group_key,
  ));
 }
}
add_action('init', 'ak_fundraiser_load_options');

// Add Fundraiser Shortcode
function ak_fundraiser_shortcode($atts, $content)
{
 if (function_exists('get_field')) {
  $style = get_field('ak_fundraiser_style', 'options');
  $style = $style ? str_replace(" ", "-", strtolower($style)) : '';
  $isDaily = get_field('ak_fundraiser_is_daily', 'options');
  $days = get_field('ak_fundraiser_daily_goals', 'options');
  $percent = get_field('ak_fundraiser_percent', 'options');
  $isShowingPeople = get_field('ak_fundraiser_is_showing_people', 'options');
  $totalPeople = get_field('ak_fundraiser_people_count', 'options');
  $peopleWording = get_field('ak_fundraiser_people_wording', 'options');
  $lastUpdated = get_field('ak_fundraiser_update_time', 'options');
  $header = get_field('ak_fundraiser_header_text', 'options');

  $output = "<div class='ak-fundraiser $style' id='ak_fundraiser'>";
  // switch ($atts->type) {
  //  case "number":
  //   $output .= '<div class="number-counter big" data-percent="' . $percent . '">0</div>';
  //   break;
  //  case "bar":
  //  default:
  if ($isDaily) {
   $now = current_datetime();
   $dow = $now->format('l');
   foreach ($days as $day) {
    if (strtolower($day['day']) == strtolower($dow)) {
     $percent = $day['percent'];
     $totalPeople = $day['people'];
     $header = $day['day'] . "'s Goal";
     break;
    }
   }
  }
  $output .= '<h4 class="fundraiser-header">' . $header . '</h4>';
  $output .= '<div class="bar-graph" data-percent="' . $percent . '">';
  $output .= '<div class="bar"></div>';
  $output .= '<div class="counter-wrapper"><span class="number-counter" data-percent="' . $percent . '">0%</span> of the goal reached</div></div>';
  if ($isShowingPeople) {
   $output .= '<div class="people-remaining">' . $totalPeople . ' ' . $peopleWording . '</div>';
  }
  $output .= '<div class="last-updated">Last updated ' . $lastUpdated . '</div>';
  //   break;
  // }
  $output .= '</div>';
  $output .= '<script>
    function checkFundraiserScroll(){
      var docViewTop = $(window).scrollTop();
      var offset = window.innerHeight * 0.75;
      var fundraisers = $(".ak-fundraiser");

      fundraisers.each(function() {
        var hasAnimated = $(this).hasClass("animated");
        if(!hasAnimated) {
          var elemTop = $(this).offset().top;

          if(elemTop <= docViewTop  + offset) {
            $(this).addClass("animated");
            var counter = $(this).find(".number-counter");
            var toAmount = counter.data("percent");
            var bar = $(this).find(".bar");

            jQuery({ Counter: 0 }).animate({ Counter: toAmount }, {
              duration: 2000,
              easing: "swing",
              step: function () {
                counter.text(Math.ceil(this.Counter) + "%");
                bar.get(0).style.setProperty("--ak-fund-filled", Math.ceil(this.Counter));
              }
            });
          }
        }
      });
    }
    $(document).unbind("scroll.akFundraiser").bind("scroll.akFundraiser", checkFundraiserScroll);
    </script>
    <style>
      :root {
        --ak-fund-counter-weight: bold;
        --ak-fund-counter-size-small: 1rem;
        --ak-fund-counter-size-large: 4rem;
        --ak-fund-border-color: #000;
        --ak-fund-fill-color: #333;
        --ak-fund-filled: 0%;
        --ak-fund-bar-height: 1rem;
        --ak-fund-bar-gap: 0.5rem;
      }

      .ak-fundraiser .number-counter {
        font-weight: var(--ak-fund-counter-weight);
        font-size: var(--ak-fund-counter-size-small);
      }
      .ak-fundraiser .number-counter.big {
        font-size: var(--ak-fund-counter-size-large);
      }

      .ak-fundraiser .bar-graph {
        display: flex;
      }
      .ak-fundraiser .bar-graph .number-counter {
        margin-right: var(--ak-fund-bar-gap);
        min-width: 4ch;
      }
      .ak-fundraiser .bar {
        flex-grow: 1;
        border: solid 1px var(--ak-fund-border-color);
        position: relative;
        overflow: hidden;
      }
      .ak-fundraiser .bar::before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        width: calc(var(--ak-fund-filled) * 1%);
        background: var(--ak-fund-fill-color);
      }
      .ak-fundraiser .fundraiser-header {
        margin-bottom: 1rem;
        text-align: center;
      }
      .ak-fundraiser .people-remaining {
        font-size: 0.8rem;
        margin-top: 1rem;
        margin-bottom: 1rem;
        text-align: center;
      }
      .ak-fundraiser .last-updated {
        font-size: 0.6rem;
        text-align: right;
        opacity: 0.3;
      }
    </style>';

  return $output;
 } else {
  return "";
 }
}
add_shortcode('ak-fundraiser', 'ak_fundraiser_shortcode');
